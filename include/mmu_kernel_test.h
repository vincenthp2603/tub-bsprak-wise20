#define KERNEL_INIT_SEC 0x001
#define KERNEL_TEXT_SEC 0x002
#define KERNEL_DATA_SEC 0x003
#define USER_TEXT_SEC 0x004
#define USER_DATA_SEC 0x005
#define KERNEL_STACK_SEC 0x07F
#define NULL 0x0

void mmu_kernel_test_read_address(unsigned int addr);
void mmu_kernel_test_write_address(unsigned int addr);
void mmu_kernel_test_jump_to_address(unsigned int addr);
void mmu_kernel_test_stack_overflow(unsigned int stack_ptr);