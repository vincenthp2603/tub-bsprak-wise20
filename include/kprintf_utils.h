#define UART_BASE (0x7E201000 - 0x3F000000)
#define STRING_BASE 0x00600004

struct argumentInfo 
{
    int inputStrlen;
    char identifier;
    int fieldWidth;
    int padding_left;
    char paddingChar;
};

//int getNumberOfArgs(char* inputStr);

struct argumentInfo fetchArgumentInfo(char* argStr);

void printStr(char* inputStr);
char* unsgnToStr(unsigned int inputNum, int base, int fieldWidth, char paddingChar, int padding_left);
char* sgnToStr(int inputNum, int base, int fieldWidth, char paddingChar, int padding_left);
char* unsgnToAddrStr(int inputNum, int fieldWidth, char paddingChar);



