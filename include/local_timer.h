#define TIMER_BASE (0x40000034)
#define INTERRUPT_EN 29
#define TIMER_EN 28
#define INTERRUPT_CLEAR 31
#define TIMER_RELOAD 30

struct timer
{
	unsigned int control_and_status;
	unsigned int clear_and_reload;
};

static volatile struct timer *const timer_port = (struct timer *)TIMER_BASE;

void init_timer();
void clear_timer_interrupt_flag();
void enable_timer_interrupt();
void disable_timer_interrupt();