#define number_of_processes 9
#define PROC_UNINIT 0
#define PROC_READY 1

void init_processes();
int get_current_process();
int find_next_process();
void swap_process(int next_process);