// Exception types
#define Reset 0
#define Undefined 1
#define Software_Interrupt 2
#define Prefetch_Abort 3
#define Data_Abort 4
#define IRQ_Interrupt 5
#define FIQ_Interrupt 6

// Modes
#define mode_user 0
#define mode_system 1
#define mode_supervisor 2
#define mode_abort 3
#define mode_fiq 4
#define mode_irq 5
#define mode_undefined 6
#define mode_invalid 7

#define thread_time 2

// Const for Heplper functions
#define N 31
#define Z 30
#define C 29
#define V 28
#define E 9
#define I 7
#define F 6
#define T 5
#define DFSR_RW 11

static volatile int irq_debug_mode = 0;

static volatile int interactive_program = 0;


void reset(unsigned int sp, unsigned int cpsr, unsigned int spsr);

unsigned int software_interrupt_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr);

unsigned int prefetch_abort_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr);

unsigned int undefined_instruction_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr);

unsigned int data_abort_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr);

unsigned int irq_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr);

unsigned int fiq_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr);

unsigned int get_lr_user();

void switch_irq_debug_mode();

void switch_interactive_program_mode();

int get_psr_mode(unsigned int psr);

char* get_exception_name(int exception_type);

char* get_psr_flags(unsigned int psr);

char* get_mode_str(int mode);

int get_pc_offset(int exception_type);

void print_data_abort_description();

void print_reg(unsigned int reg);
