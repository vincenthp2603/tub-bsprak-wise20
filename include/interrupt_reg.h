#define INTERRUPT_REG_BASE (0x7E00B000 - 0x3F000000 + 0x200)
#define IRQ_TIMER_EN 0
#define IRQ_UART_EN 25
#define IRQ_UART_DIS 25
#define IRQ_UART 25

struct interrupt_reg
{
    unsigned int irq_basic_pending;
    unsigned int irq_pending_1;
    unsigned int irq_pending_2;
    unsigned int fiq_control;
    unsigned int enable_irq_1;
    unsigned int enable_irq_2;
    unsigned int enable_basic_irq;
    unsigned int disable_irq_1;
    unsigned int disable_irq_2;
    unsigned int disable_basic_irq;
};

static volatile struct interrupt_reg *const interrupt_port = (struct interrupt_reg *)INTERRUPT_REG_BASE;

void enable_irq();

void disable_uart_irq();