#define VIR_USR_STACK_BASE_ADDR 0x1B00000
#define USR_MODE_STACK_PTR 0x1AFFFFF

#define MMU_EN 0
#define ALIGNMENT_CHECK_EN 1
#define I_CACHE_EN 12
#define C_CACHE_EN 2

#define DACR_CLIENT 1         //set dacr to client
#define TTBCR_USE_TTBR0 0

#define L1_TABLE_SIZE 4096
#define L1_ALIGNMENT 4096*4
#define L2_TABLE_SIZE 256
#define L2_ALIGNMENT 256*4

#define KERNEL_STACK_SEC 0x07F
#define KERNEL_INIT_SEC 0x001
#define KERNEL_TEXT_SEC 0x002
#define KERNEL_DATA_SEC 0x003

#define USER_TEXT_SEC 0x004
#define USER_RODATA_SEC 0x005
#define VIR_USER_DATA_SEC 0x006
#define VIR_USER_BSS_SEC 0x010
#define VIR_USER_STACK_SEC (VIR_USR_STACK_BASE_ADDR >> 20)
#define USER_MODE_STACK_SEC (USR_MODE_STACK_PTR >> 20) 

#define PERIPHERAL_SEC_0 0x3F0 // interrupt register
#define PERIPHERAL_SEC_1 0x3F2 // uart
#define PERIPHERAL_SEC_2 0x400 // local timer

#define L1_SECTION_TYPE 1
#define L1_SEC_TYPE_XN 4
#define L1_SEC_TYPE_PXN 0
#define L1_SEC_TYPE_AP0 10
#define L1_SEC_TYPE_AP1 11
#define L1_SEC_TYPE_AP2 15

#define L1_PTR_TYPE 0
#define L1_PTR_TYPE_PXN 2 

#define L2_PAGE_PTR_TYPE 1
#define L2_AP0 4
#define L2_AP1 5
#define L2_AP2 9
#define L2_XN 0

void init_mmu();

int get_phys_user_stack_sec(int index);

unsigned int get_L1_table_base_addr();

unsigned int get_L2_table_base_addr(int index);

void L1_init_section_entry(unsigned int index);

void L1_init_L2_pointer_entry(unsigned int index, unsigned int L2_base_addr);

void L1_invalidate_entry(unsigned int index);

void L1_set_fullAccess_section(unsigned int index);

void L1_set_sys_rw_section(unsigned int index);

void L1_set_sys_r_section(unsigned int index);

void L1_set_execNever_section(unsigned int index);

void L1_set_privileged_execNever_section(unsigned int index);

void L1_set_usr_r_section(unsigned int index);

void L1_set_privileged_execNever_ptr(unsigned int index);

void L2_init_entry(unsigned int table_index, unsigned int entry_index);

void L2_invalidate_entry(unsigned int table_index, unsigned int entry_index);

void L2_set_fullAccess_entry(unsigned int table_index, unsigned int entry_index);

void L2_set_execNever_entry(unsigned int table_index, unsigned entry_index);

void setup_L2_table_of_process(int index);