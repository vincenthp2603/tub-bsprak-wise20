#define number_of_threads 33
#define BEREIT 0
#define LAUFEND 1
#define WARTEND 2
#define BEENDET 3
#define UNINIT 4
#define IDLE_THREAD 0
#define USER_STACK_BASE_ADDR 0x1B00000
#define USER_MODE_SP 0x1AFFFFF
#include <kprintf.h>

typedef struct _thread_control_block
{
    unsigned int r0;
    unsigned int r1;
    unsigned int r2;
    unsigned int r3;
    unsigned int r4;
    unsigned int r5;
    unsigned int r6;
    unsigned int r7;
    unsigned int r8;
    unsigned int r9;
    unsigned int r10;
    unsigned int r11;
    unsigned int r12;
    unsigned int r13;
    unsigned int lr_handler;
    unsigned int r15;
    unsigned int lr_usr;
    unsigned int sp_usr;
    unsigned int thread_sp;
    unsigned int cpsr;
    unsigned int state;
    unsigned int wait_time;
    unsigned int process_id;
} tcb;

void init_tcbs();

tcb *get_tcbs_list();

int get_running_thread();

int find_free_tcb();

int thread_create(void (*func)(void*), void *args, unsigned int args_size);

void setup_first_user_thread_in_process(unsigned int thread_id, void *args, unsigned int args_size);

int get_thread_state(int thread_id);

void save_context(unsigned int handler_sp, unsigned int spsr);

unsigned int load_context(unsigned int next_thread_id, unsigned int handler_sp);

void init_idle_thread();

void init_creator_thread();

int find_next_thread();

unsigned int swap_thread(unsigned int handler_sp, unsigned int spsr);

void end_thread();