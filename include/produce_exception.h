void supervisor_call() {
    asm ("swi 99");
};

void produce_prefetch_abort() {
    asm ("bkpt #16");
};

void produce_undefined_instruction() {
    asm ("udf");
};