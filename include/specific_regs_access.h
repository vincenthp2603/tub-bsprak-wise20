unsigned int get_dfar();

unsigned int get_dfsr();

unsigned int get_lr_user();

unsigned int get_sp_user();

unsigned int get_lr_supervisor();

unsigned int get_sp_supervisor();

unsigned int get_spsr_supervisor();

unsigned int get_lr_abort();

unsigned int get_sp_abort();

unsigned int get_spsr_abort();

unsigned int get_lr_fiq();

unsigned int get_sp_fiq();

unsigned int get_spsr_fiq();

unsigned int get_lr_irq();

unsigned int get_sp_irq();

unsigned int get_spsr_irq();

unsigned int get_lr_undefined();

unsigned int get_sp_undefined();

unsigned int get_spsr_undefined();

unsigned int get_lr();

unsigned int get_sp();

unsigned int get_spsr();