#define UART_BASE (0x7E201000 - 0x3F000000)
#define WLEN 5
#define BUSY 3
#define RXE 9
#define TXE 8
#define FEN 4
#define RXFE 4
#define TXFF 5
#define RXIM 4
#define TXIM 5

struct uart
{
	unsigned int data;
	unsigned int status;
	unsigned int unused0[4];
	unsigned int flag;
	unsigned int unused1[4];
	unsigned int lineCtrl;
	unsigned int ctrl;
	unsigned int IFLS;
	unsigned int IMSC;
};

static volatile struct uart *const uart_port = (struct uart *)UART_BASE;


void start_uart_routine(void);

void uart_init();

void uart_enable_reception();

void uart_enable_transmission();

unsigned int uart_receive();

void uart_transmit(char a);

void interrupt_driven_uart_setup();

unsigned int interrupt_driven_uart_receive();

void interrupt_driven_uart_transmit(unsigned int a);




