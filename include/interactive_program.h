#include <config.h>

void start_interactive_program();

void pull_buffer();

void push_buffer(unsigned int c);

void interactive_program_pause();

void interactive_program_print(unsigned int c);

void test_buffer();
