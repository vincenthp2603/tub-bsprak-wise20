void syscall_end_thread();
void syscall_sleep_thread(unsigned int wait_time);
void syscall_create_thread(void (*func)(void*), void *args, unsigned int args_size);
void syscall_uart_print(char c);
void syscall_create_process(void (*func)(void*), void *args, unsigned int args_size);
unsigned int syscall_uart_read();
void syscall_unknown();