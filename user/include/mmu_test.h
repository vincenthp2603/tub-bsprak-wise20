#define KERNEL_INIT_SEC 0x001
#define KERNEL_TEXT_SEC 0x002
#define KERNEL_DATA_SEC 0x003
#define USER_TEXT_SEC 0x004
#define USER_DATA_SEC 0x005
#define KERNEL_STACK_SEC 0x07F
#define USER_MODE_STACK_SEC (USER_MODE_SP >> 20) - 1 

#define PERIPHERAL_SEC_0 0x3F0 // interrupt register
#define PERIPHERAL_SEC_1 0x3F2 // uart
#define PERIPHERAL_SEC_2 0x400 // local timer

void mmu_test_read_address(unsigned int addr);
void mmu_test_write_address(unsigned int addr);
void mmu_test_jump_to_address(unsigned int addr);
void mmu_test_stack_overflow(unsigned int stack_ptr);