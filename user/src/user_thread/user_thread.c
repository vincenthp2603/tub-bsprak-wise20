#include <syscall.h>
#include <usr_config.h>
#include <uprintf.h>
#include <mmu_test.h>

#define NULL 0

unsigned int global_ctr;

void user_thread_active_wait()
{
    for (int i = 0; i < 200 * BUSY_WAIT_COUNTER; i++)
    {
        asm("nop");
    };
};

void active_user_thread_job(void *x)
{
    char c = *(char *)x;

    for (int i = 0; i < 8; i++)
    {
        syscall_uart_print(c);
        user_thread_active_wait();
    };
    syscall_end_thread();
}

void passive_user_thread_job(void *x)
{
    char c = *(char *)x;

    for (int i = 0; i < 5; i++)
    {
        syscall_uart_print(c);
        syscall_sleep_thread(3);
    }

    syscall_end_thread();
}

void user_thread_job(void* x) {
    char c = *((char*) x);
    unsigned int thread_identifier = (unsigned int) *((char*) x + 1);

    for (int i = 0; i < 20; i++)
    {
        uprintf("%c:%u (%u:%u) \n", c, global_ctr, thread_identifier, i);
        global_ctr++;
        syscall_sleep_thread(3);
    }    

    syscall_end_thread();
}

void create_user_thread(char input, char thread_identifier) {
    char data[] = { input, thread_identifier};
    syscall_create_thread((void *)&user_thread_job, (void*) data, 2); 
};

void first_thread_in_process_job(void *x)
{
    char thread_identifier = 1;    
    char c = *(char *)x;

    global_ctr = 100;
    
    create_user_thread(c, thread_identifier + 1);
    create_user_thread(c, thread_identifier + 2);

    for (int i = 0; i < 20; i++)
    {
        uprintf("%c:%u (%u:%u) \n", c, global_ctr, thread_identifier, i);
        global_ctr++;
        syscall_sleep_thread(3);
    }

    syscall_end_thread();

}

void (*func_ptr)(void *);

void creator_thread_job()
{
    //uprintf("Creator starts \n");

    while (1)
    {
        char input = (char)syscall_uart_read();
        while (input != 0)
        {
            func_ptr = (void *)&first_thread_in_process_job;
            syscall_create_process(func_ptr, (void *)&input, 1);
            input = (char)syscall_uart_read();
        }

        syscall_sleep_thread(1);
    }
}

void idle_thread_job(void *someptr)
{
    //uprintf("Idle Thread starts \n");
    (unsigned int *)someptr++;
    someptr--;
    while (1)
    {
        //asm("wfi");
    };
};