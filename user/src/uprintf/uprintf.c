#include <stdarg.h>
#include <syscall.h>

char buffer[50];

void user_printStr(char* str) {
    int i = 0;
    while(str[i] != 0)
    {
        syscall_uart_print(str[i]);
        i++;
    }
}

char* numToString(unsigned int num, int base) {
    char digitArr[16] = "0123456789abcdef";
    for(int i = 0; i < 20; i++) buffer[i] = 0;
    unsigned int factor = 1;
    while(factor <= num/base) factor = factor*base;
    int i = 0;
    while(factor > 0)
    {
        buffer[i] = digitArr[num/factor];
        num = num % factor;
        factor = factor/base;
        i++;
    }
    return buffer;
}

void uprintf(char *inputStr, ...)
{
    va_list argPtr;
    va_start(argPtr, inputStr);
    int i = 0;

    while (inputStr[i] != 0)
    {
        if (inputStr[i] == '%')
        {
            if (inputStr[i + 1] == '%')
            {
                syscall_uart_print('%');
            }
            else
            {
                char identifier = inputStr[i + 1];
                char outputChar;
                char *outputStr;
                unsigned int inputNum;

                switch (identifier)
                {
                case 'c':
                    outputChar = va_arg(argPtr,int);
                    syscall_uart_print(outputChar);
                    break;
                
                case 's':
                    outputStr = va_arg(argPtr,char*);
                    user_printStr(outputStr);
                    break;

                case 'u':
                    inputNum = va_arg(argPtr,unsigned int);
                    outputStr = numToString(inputNum,10);
                    user_printStr(outputStr);
                    break;

                case 'x':
                    inputNum = va_arg(argPtr,unsigned int);
                    outputStr = numToString(inputNum,16);
                    user_printStr(outputStr);
                    break;

                default:
                    break;
                }
            }
            i += 2;
            continue;
        }

        syscall_uart_print(inputStr[i]);
        i++;
    }
    
    va_end(argPtr);
}
