int strlen(char* str) {
    char* ptr = str;
    int len = 0;
    while (*ptr != 0)
    {
        len++;
        ptr++;
    }
    return len;
};

int strcmp(char* str1, char* str2) {
    if (strlen(str1) != strlen(str2)) return 0;
    for (int i = 0; i < strlen(str1); i++) {
        if ( str1[i] != str2[i] ) return 0;
    };
    return 1;
};

