#define UART_BASE (0x7E201000 - 0x3F000000)
#define PERCENT_SYMBOL 37
#define SPACE 32
#define NEW_LINE 10
#define BACK_SLASH 92
#include <stdarg.h>
#include <kprintf_utils.h>
#include <uart.h>
#include <string.h>

void kprintf(char* inputStr, ...) {
   
    va_list argPtr;
    va_start(argPtr, inputStr);
    int i = 0;
    while (inputStr[i] != 0) {
        if(inputStr[i] == PERCENT_SYMBOL) {
            if(inputStr[i+1] == PERCENT_SYMBOL) {
                uart_transmit(PERCENT_SYMBOL);
                i += 2;
            } else {
                struct argumentInfo argInfo = fetchArgumentInfo(&inputStr[i]);
                char identifier = argInfo.identifier;
                int fieldWidth = argInfo.fieldWidth;
                char paddingChar = argInfo.paddingChar;
                int padding_left = argInfo.padding_left;

                char outputChar;
                char* outputStr;
                unsigned int usgnInputNum;
                int sgnInputNum;
                int len;
                void* ptr;

                switch (identifier)
                {
                case 'c':
                    outputChar = va_arg(argPtr,int);
                    if (fieldWidth > 1 && padding_left) for (int i = 0; i < fieldWidth-1; i++) uart_transmit(SPACE);
                    uart_transmit(outputChar);
                    if (fieldWidth > 1 && !padding_left) for (int i = 0; i < fieldWidth-1; i++) uart_transmit(SPACE);
                    break;
                
                case 's':
                    outputStr = va_arg(argPtr,char*);
                    len = strlen(outputStr);
                    if (fieldWidth > len && padding_left) for (int i = 0; i < fieldWidth-len; i++) uart_transmit(SPACE);
                    printStr(outputStr);
                    if (fieldWidth > len && !padding_left) for (int i = 0; i < fieldWidth-len; i++) uart_transmit(SPACE);
                    break;

                case 'x':
                    usgnInputNum = va_arg(argPtr, unsigned int);
                    outputStr = unsgnToStr(usgnInputNum,16,fieldWidth,paddingChar,padding_left);
                    printStr(outputStr);            
                    break;

                case 'u':
                    usgnInputNum = va_arg(argPtr, unsigned int);
                    outputStr = unsgnToStr(usgnInputNum,10,fieldWidth,paddingChar,padding_left);
                    printStr(outputStr);
                    break;
                
                case 'i':
                    sgnInputNum = va_arg(argPtr, int);
                    outputStr = sgnToStr(sgnInputNum,10,fieldWidth,paddingChar,padding_left);
                    printStr(outputStr);
                    break;
                
                case 'p':
                    ptr = va_arg(argPtr, void*);
                    usgnInputNum = (unsigned int) ptr; 
                    outputStr = unsgnToAddrStr(usgnInputNum,fieldWidth,paddingChar);
                    printStr(outputStr);
                    break;

                default:
                    break;
                }

                i += argInfo.inputStrlen;
            }
            continue;
        }

        uart_transmit(inputStr[i]);
        i++;
    }
    va_end(argPtr);
}

void testStrAddr(char* str)
{
    kprintf("%p ", str);
};