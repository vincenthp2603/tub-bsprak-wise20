#define UART_BASE (0x7E201000 - 0x3F000000)
#define PERCENT_SYMBOL 37
#define ASTERIK_SYMBOL 42
#include "./kprintf_utils.h"
#include <uart.h>

unsigned int STRINGBASE = 1;
//static char* outputStr = (char*) &STRINGBASE;
//static char* outputStr = (char*) STRING_BASE;
static char outputStr[128];

struct argumentInfo fetchArgumentInfo(char* argStr) {
    struct argumentInfo argInfo;
    int i = 0;
	int fieldWidth = 0;
    int fieldWidthStrlen = 0;
    int padding_left = 1;
    int startPosition = 1;
    int factor = 0;

    while (argStr[i] < 97 || argStr[i] > 122  || argStr[i] == '-')
    {
        if (argStr[i] == '-') {
            padding_left = 0;
            fieldWidthStrlen++;
            i++;
            continue;
        };

        if (argStr[i] >= 48 && argStr[i] <= 57) {
           factor = factor == 0 ? 1 : factor*10;
           fieldWidthStrlen++;
        };
        i++;
    };

    argInfo.padding_left = padding_left;
    argInfo.identifier = argStr[i];
    argInfo.inputStrlen = i + 1;

    if (!fieldWidthStrlen) {
        argInfo.fieldWidth = -1;
    } if (fieldWidthStrlen == 1) {
        argInfo.fieldWidth = argStr[1] - 48;
    } else {
        for (int i = startPosition; i < startPosition + fieldWidthStrlen; i++) {
            if (argStr[i] == '-') {
                continue;
            };
            fieldWidth = fieldWidth + (argStr[i] - 48)*factor;
            factor = factor/10;
        };
        argInfo.fieldWidth = fieldWidth;
    };

    if (fieldWidthStrlen >= 2 && argStr[1] == '0') 
    {
        argInfo.paddingChar = 48;
    }
    else
    {
        argInfo.paddingChar = 32;
    }
    return argInfo;
};

void printStr(char* inputStr) {
    int i = 0;
    while (inputStr[i] != 0)
    {
        uart_transmit(inputStr[i]);
        i++;
    }    
};

char* unsgnToStr(unsigned int inputNum, int base, int fieldWidth, char paddingChar, int padding_left) {
    char digitArr[16] = "0123456789abcdef";
    int len = 0;
    int offset = 0;
 
    unsigned int temp = inputNum;
    while(temp || !len) {
        temp = temp/base;
        len++;
    };
     
    if (fieldWidth > len) {
        offset = fieldWidth - len;
        if (padding_left) for (int i = 0; i < offset; i++) outputStr[i] = paddingChar;
        if (!padding_left) for (int i = len; i < len + offset; i++) outputStr[i] = paddingChar;        
    };

    int factor = 1;
    int index;
    temp = inputNum;
    
    for (int i = 0; i < len-1; i++) factor = factor*base;
    for (int i = 0; i <= len-1; i++) {
        index = temp/factor;
        if (padding_left) outputStr[i+offset] = digitArr[index];
        if (!padding_left) outputStr[i] = digitArr[index];
        temp = temp - index*factor;
        factor = factor/base;
    };

    outputStr[len+offset] = 0;    
    
    return outputStr;
};

char* sgnToStr(int inputNum, int base, int fieldWidth, char paddingChar, int padding_left) {
    char digitArr[16] = "0123456789abcdef";
    int len = 0;
    int offset = 0;
    
    int temp = inputNum < 0 ? -inputNum : inputNum;
    int isNegative = inputNum < 0 ? 1 : 0;

    while(temp || !len) {
        temp = temp/base;
        len++;
    };
    if (isNegative) len++;

    if (fieldWidth > len) {
        offset = fieldWidth - len;
        if (padding_left) for (int i = 0; i < offset; i++) outputStr[i] = paddingChar;
        if (!padding_left) for (int i = len; i < len + offset; i++) outputStr[i] = paddingChar;
    };

    int factor = 1;
    int index;
    temp = inputNum < 0 ? -inputNum : inputNum;
    
    for (int i = 0; i < len-1-isNegative; i++) factor = factor*base;
    for (int i = 0; i <= len-1; i++) {
        if (isNegative && i == 0) {
            if (padding_left) outputStr[i+offset] = '-';
            if (padding_left) outputStr[i] = '-';
            continue;
        };
        index = temp/factor;
        if (padding_left) outputStr[i+offset] = digitArr[index];
        if (!padding_left) outputStr[i] = digitArr[index];
        temp = temp - index*factor;
        factor = factor/base;
    };

    outputStr[len+offset] = 0;    
    
    return outputStr;

};

char* unsgnToAddrStr(int inputNum, int fieldWidth, char paddingChar) {
    char digitArr[16] = "0123456789abcdef";
    int offset = 0;
    int len = 0;

    outputStr[offset] = '0';
    outputStr[offset+1] = 'x';

    int temp = inputNum;
    while(temp || !len) {
        temp = temp/16;
        len++;
    };
     
    if (fieldWidth > len + 2) {
        offset = fieldWidth - len -2;
        for (int i = 0; i < offset; i++) outputStr[i] = paddingChar;
    };

    int factor = 1;
    int index;
    temp = inputNum;

    for (int i = 0; i < len - 1; i++) factor = factor*16;
    for (int i = 0; i <= len - 1; i++) {
        index = temp/factor;
        outputStr[i+offset+2] = digitArr[index];
        temp = temp - index*factor;
        factor = factor/16;
    };

    outputStr[len+offset+2] = 0;    
    
    return outputStr;
    
};


