void memcpy(void* src, void* dst, int size)
{
    char *src_ptr = (char*) src;
    char *dst_ptr = (char*) dst;
    for (int i = 0; i < size; i++)
    {
        *dst_ptr = *src_ptr;
        src_ptr++;
        dst_ptr++;
    };
};

void memcpy_to_descending_stack(void* src, void** stack_pointer, int size)
{
    char *src_ptr = (char*) src;
    char *stack_ptr = (char*) *stack_pointer;
    stack_ptr -= size;
    for (int i = 0; i < size; i++)
    {
        *stack_ptr = *src_ptr;
        src_ptr++;
        stack_ptr++;
    };
    *stack_pointer -= size;
}