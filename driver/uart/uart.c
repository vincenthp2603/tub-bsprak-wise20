#include <uart.h>
#include <interrupt_reg.h>
#include <kprintf.h>

void uart_init()
{
	uart_port->ctrl = 0;
	while (((uart_port->flag >> BUSY) & 1) == 1)
	{
	};
	uart_port->lineCtrl = (0b11 << WLEN) + (1 << FEN); 
	uart_port->ctrl = (1 << TXE) + 1;
}

void uart_enable_reception()
{
	uart_port->ctrl = 0;								//disable uart
	while (((uart_port->flag >> BUSY) & 1) == 1)
	{
	};									  				//wait for the end of transmission
	uart_port->lineCtrl = (0b11 << WLEN); 				//Set FEN bit to 0, flush the transmit FIFO
	uart_port->ctrl = (1 << RXE) + 1;	  				//enable Reception
}

void uart_enable_transmission()
{
	uart_port->ctrl = 0; 								//disable uart
	while (((uart_port->flag >> BUSY) & 1) == 1)
	{
	};												   	//wait for the end of reception
	uart_port->lineCtrl = (0b11 << WLEN) + (1 << FEN); 	//Set FEN bit to 1
	uart_port->ctrl = (1 << TXE) + 1;				   	//enable Transmission
}

unsigned int uart_receive()
{
	while (((uart_port->flag >> RXFE) & 1) == 1)		//Receive FIFO empty
	{
	}; 
	return uart_port->data;
}

void uart_transmit(char a)
{
	while (((uart_port->flag >> TXFF) & 1) == 1)		//Transmit FIFO empty
	{
	};
	uart_port->data = (int)a;
}

void interrupt_driven_uart_setup()
{
	uart_port->ctrl &= ~(1);                            // Disable uart
    uart_port->lineCtrl = (0b11 << WLEN) & ~(1 << FEN); // Disable FIFO
    uart_port->ctrl |= 1;                               // Enable uart
    //uart_port->IMSC = 1 << RXIM | 1 << TXIM;
	uart_port->IMSC = 1 << RXIM;
}

unsigned int interrupt_driven_uart_receive()
{
	unsigned int input;
	if (interrupt_port->irq_pending_2 & (1 << IRQ_UART))
    {
        //kprintf("UART Interrupt\n");
		input = uart_port->data; 
		interrupt_port->enable_irq_2 = 1 << IRQ_UART;
		return input;
    };
	return 0;
}

void interrupt_driven_uart_transmit(unsigned int a)
{
	if ((interrupt_port->irq_pending_2 & (1 << IRQ_UART)))
    {
		uart_port->data = a; 
		interrupt_port->enable_irq_2 = 1 << IRQ_UART;
    };
	return;
}