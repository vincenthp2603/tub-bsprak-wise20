#include <local_timer.h>
#include <config.h>
#include <interrupt_reg.h>

void init_timer()
{
    timer_port->control_and_status = 0 | 1 << INTERRUPT_EN;
    timer_port->control_and_status |= 50*LOCAL_TIMER_US;
    timer_port->control_and_status |= 1 << TIMER_EN;
};

void clear_timer_interrupt_flag()
{
    timer_port->clear_and_reload = 1 << INTERRUPT_CLEAR;
}

void enable_timer_interrupt()
{
    timer_port->control_and_status = 0 | 1 << INTERRUPT_EN;
    timer_port->control_and_status |= 50*LOCAL_TIMER_US; //default 50
    timer_port->control_and_status |= 1 << TIMER_EN;
}

void disable_timer_interrupt()
{
    timer_port->control_and_status = 0;
}