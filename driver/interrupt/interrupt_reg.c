#include <interrupt_reg.h>

void enable_timer_irq()
{
    interrupt_port->enable_basic_irq = (1 << IRQ_TIMER_EN);
};

void enable_uart_irq()
{
    interrupt_port->enable_irq_2 = (1 << IRQ_UART_EN);
};

void enable_irq() 
{
    //enable_timer_irq();
    enable_uart_irq();
};

void disable_uart_irq()
{
    interrupt_port->enable_irq_2 &= ~(1 << IRQ_UART_EN); 
} 

