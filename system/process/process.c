#include <mem.h>
#include <mmu.h>
#include <mmu_controller.h>
#include <process.h>
#include <kprintf.h>

#define IDLE_PROCESS 0

#define PROC_UNINIT 0
#define PROC_READY 1
#define PROC_RUNNING 2

static int current_process = 0;
static int process_state[number_of_processes];

int get_current_process() {
    return current_process;
}

int get_current_process_state() {
   return process_state[current_process];
};

int find_next_process() {
    
    //kprintf("Current Process is %i \n", current_process);
    
    int i = (current_process == number_of_processes-1) ? 1 : current_process + 1;
    
    //kprintf("Current process is %i \n", current_process);

    while (i != current_process)
    {
        if (process_state[i] == PROC_UNINIT) return i;
        i = (i == (number_of_processes-1)) ? 1 : (i+1);
    };    

    kprintf("Process List is full !!!!\n");
    return -1;
};

void swap_process(int next_process) {
    
    //kprintf("Swap process %i -> %i\n", current_process, next_process);

    if (next_process != current_process) {     

        process_state[current_process] = PROC_READY;
        process_state[next_process] = PROC_RUNNING;

        current_process = next_process;
        
        L1_init_section_entry(VIR_USER_DATA_SEC);
        L1_set_fullAccess_section(VIR_USER_DATA_SEC);
        L1_set_execNever_section(VIR_USER_DATA_SEC);

        L1_init_section_entry(VIR_USER_BSS_SEC);
        L1_set_fullAccess_section(VIR_USER_BSS_SEC);
        L1_set_execNever_section(VIR_USER_BSS_SEC);

        L1_init_L2_pointer_entry(VIR_USER_STACK_SEC, get_L2_table_base_addr(current_process));
        L1_set_privileged_execNever_ptr(VIR_USER_STACK_SEC);
        setup_L2_table_of_process(current_process);

        flush_tlb();
    };
}

void init_processes() 
{
    process_state[IDLE_PROCESS] = PROC_RUNNING;
    for (int i = 1; i < number_of_processes; i++) process_state[i] = PROC_UNINIT; 
    current_process = IDLE_PROCESS;
}


