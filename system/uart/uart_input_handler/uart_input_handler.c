#include <uart.h>
#include <kprintf.h>
#include <interrupt_reg.h>
#include <config.h>
#include <handler.h>

static unsigned int uart_buffer[UART_INPUT_BUFFER_SIZE];

unsigned int pop_uart_buffer()
{
    unsigned int to_print = uart_buffer[0];
    for (int i = 0; i < UART_INPUT_BUFFER_SIZE; i++)
    {
        if (i != UART_INPUT_BUFFER_SIZE - 1)
        {
            uart_buffer[i] = uart_buffer[i+1];
        }
        else
        {
            uart_buffer[i] = 0;
        };        
    };
    return to_print;
};

void push_uart_buffer(unsigned int c)
{
    if (c == 0) return; 
    for (int i = 0; i < UART_INPUT_BUFFER_SIZE; i++)
    {
        if (uart_buffer[i] == 0) {
            uart_buffer[i] = c;
            //kprintf("Push %c to %u\n", (char) c, (unsigned int) i);
            break;
        };
    };    
};