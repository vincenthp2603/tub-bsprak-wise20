#include <uart.h>
#include <kprintf.h>
#include <interrupt_reg.h>
#include <config.h>
#include <handler.h>
#include <interactive_program.h>


static unsigned int buffer[UART_INPUT_BUFFER_SIZE];

static int printing = 0;

void pull_buffer()
{
    for (int i = 0; i < UART_INPUT_BUFFER_SIZE; i++)
    {
        if (i != UART_INPUT_BUFFER_SIZE - 1)
        {
            buffer[i] = buffer[i+1];
        }
        else
        {
            buffer[i] = 0;
        };        
    };
};

void push_buffer(unsigned int c)
{
    if (c == 0) return; 
    for (int i = 0; i < UART_INPUT_BUFFER_SIZE; i++)
    {
        if (buffer[i] == 0) {
            buffer[i] = c;
            break;
        };
    };    
};

void interactive_program_pause()
{
    for (int i = 0; i < 100*BUSY_WAIT_COUNTER; i++) {
        asm("nop"); 
    };
};

void interactive_program_print(unsigned int c)
{
    printing = 1;
    pull_buffer();
    for (int i = 0; i < 20; i++)
    {
        uart_port->data = c;
        interactive_program_pause();
    };
    printing = 0;
};

void start_interactive_program()
{
    switch_interactive_program_mode();
    kprintf("\n###################### Interaktives Programm startet #######################\n");
    interrupt_driven_uart_setup();
    
    while (1)
    {  
        interactive_program_print(buffer[0]);
    };
};

void test_buffer()
{
    kprintf("Test Buffer: ");
    for (int i = 0; i < 10 ; i++)
    {
        unsigned int c = buffer[i];
        kprintf("%c ", (char) c);
    };
    kprintf("\n");
};