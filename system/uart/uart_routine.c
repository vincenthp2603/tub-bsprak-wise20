#include <uart.h>
#include <kprintf.h>
#include <produce_exception.h>
#include <local_timer.h>
#include <interrupt_reg.h>
#include <handler.h>
#include <interactive_program.h>
#include <regcheck.h>

void start_uart_routine() {
	kprintf("\n");
	kprintf("############################################################################\n");
	kprintf("##### Drücken s, um ein Supervisor Call auszulösen.                    #####\n");
	kprintf("##### Drücken u, um eine Undefined Instruction durchzuführen.          #####\n");
	kprintf("##### Drücken p, um einen Prefetch Abort auszulösen.                   #####\n");
	kprintf("##### Drücken d, um den IRQ Debug Mode umzuschalten (default ist aus). #####\n");
	kprintf("##### Drücken e, um das Unterprogramm aufzurufen.                      #####\n");
	kprintf("##### Drücken c, um register_checker() durchzuführen.                  #####\n");
	kprintf("############################################################################\n");
	
	while (1)
	{
		uart_enable_reception();
		
		unsigned int c = uart_receive();
		switch (c)
		{
		case 's':
			supervisor_call();
			break;
		
		case 'u':
			produce_undefined_instruction();
			break;

		case 'p':
			produce_prefetch_abort();
			break;
		
		case 'd':
			switch_irq_debug_mode();
			break;

		case 'e':
			start_interactive_program();
			break;

		case 'c':
			register_checker();
			break;

		default:
			break;
		};

		uart_enable_transmission();
	}
}