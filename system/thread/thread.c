#include <thread.h>
#include <handler.h>
#include <kprintf.h>
#include <mem.h>
#include <mmu_controller.h>
#include <mmu.h>
#include "../../user/include/user_thread.h"
#include <process.h>
#define USER_MODE 0x10

static int running_thread = -1;

static tcb thread[number_of_threads];

void init_tcbs()
{
    for (int i = 0; i < number_of_threads; i++)
    {
        thread[i].r0 = 0;
        thread[i].r1 = 0;
        thread[i].r2 = 0;
        thread[i].r3 = 0;
        thread[i].r4 = 0;
        thread[i].r6 = 0;
        thread[i].r5 = 0;
        thread[i].r7 = 0;
        thread[i].r8 = 0;
        thread[i].r9 = 0;
        thread[i].r10 = 0;
        thread[i].r11 = 0;
        thread[i].r12 = 0;
        thread[i].r13 = 0;
        thread[i].lr_handler = 0;
        thread[i].r15 = 0;
        thread[i].lr_usr = 0;
        thread[i].sp_usr = USER_MODE_SP;      
        thread[i].thread_sp = USER_STACK_BASE_ADDR + i*1024; // Virtual Address -> Will be mapped to different Physical Address          
        thread[i].cpsr = USER_MODE;        
        thread[i].state = UNINIT;
        thread[i].wait_time = 0;
        thread[i].process_id = (i <= 1) ? i : -1;
    };
};

tcb *get_tcbs_list()
{
    return thread;
};

int get_running_thread()
{
    return running_thread;
};

void end_thread()
{
    if (running_thread != IDLE_THREAD)
    {
        thread[running_thread].state = BEENDET;
    }
    while (1) { };    
};

int find_free_tcb()
{
    for (int i = 0; i < number_of_threads-1; i++)
    {
        if (thread[i].state == UNINIT) return i;       
    };
    return -1;
};

int thread_create(void (*func)(void*), void *args, unsigned int args_size) 
{
    int index = find_free_tcb();

    if (index != -1)
    {
        thread[index].state = BEREIT;
        thread[index].lr_handler = (unsigned int) func;
        memcpy(args, (void*) thread[index].thread_sp, args_size);
                
        if (index != IDLE_THREAD)
        {
            thread[index].r0 = thread[index].thread_sp;  // Pass args to thread job
            //char c = *((char*) thread[index].thread_sp);
            //kprintf("Test: %c\n", c);     
        };
        return index;
    };
    
    kprintf("Thread list full!!!");
    return -1;
};

void setup_first_user_thread_in_process(unsigned int thread_id, void *args, unsigned int args_size)
{
    memcpy(args, (void*) (thread[thread_id].thread_sp + (thread[thread_id].process_id + 1)*0x100000), args_size);
    memcpy((void*) (VIR_USER_DATA_SEC<<20), (void*) ((VIR_USER_DATA_SEC + thread[thread_id].process_id + 1) << 20), 1024*1024);
}

int get_thread_state(int thread_id)
{
    return thread[thread_id].state;
};

void save_context(unsigned int handler_sp, unsigned int spsr)
{
    int index = running_thread;
    thread[index].r0 = *((unsigned int*) handler_sp);
    thread[index].r1 = *((unsigned int*) handler_sp+1);
    thread[index].r2 = *((unsigned int*) handler_sp+2);
    thread[index].r3 = *((unsigned int*) handler_sp+3);
    thread[index].r4 = *((unsigned int*) handler_sp+4);
    thread[index].r5 = *((unsigned int*) handler_sp+5);
    thread[index].r6 = *((unsigned int*) handler_sp+6);
    thread[index].r7 = *((unsigned int*) handler_sp+7);
    thread[index].r8 = *((unsigned int*) handler_sp+8);
    thread[index].r9 = *((unsigned int*) handler_sp+9);
    thread[index].r10 = *((unsigned int*) handler_sp+10);
    thread[index].r11 = *((unsigned int*) handler_sp+11);
    thread[index].r12 = *((unsigned int*) handler_sp+12);
    thread[index].r13 = *((unsigned int*) handler_sp+13);
    thread[index].lr_handler = *((unsigned int*) handler_sp+14); // store the actual pc of threads      
    thread[index].r15 = *((unsigned int*) handler_sp+15);
    thread[index].lr_usr = *((unsigned int*) handler_sp+16);
    thread[index].sp_usr = *((unsigned int*) handler_sp+17);
    thread[index].cpsr = spsr;                                     
};

unsigned int load_context(unsigned int next_thread_id, unsigned int handler_sp)
{
    int index = next_thread_id;
    *((unsigned int*) handler_sp) = thread[index].r0;
    *((unsigned int*) handler_sp+1) = thread[index].r1;
    *((unsigned int*) handler_sp+2) = thread[index].r2;
    *((unsigned int*) handler_sp+3) = thread[index].r3;
    *((unsigned int*) handler_sp+4) = thread[index].r4;
    *((unsigned int*) handler_sp+5) = thread[index].r5;
    *((unsigned int*) handler_sp+6) = thread[index].r6;
    *((unsigned int*) handler_sp+7) = thread[index].r7;
    *((unsigned int*) handler_sp+8) = thread[index].r8;
    *((unsigned int*) handler_sp+9) = thread[index].r9;
    *((unsigned int*) handler_sp+10) = thread[index].r10;
    *((unsigned int*) handler_sp+11) = thread[index].r11;
    *((unsigned int*) handler_sp+12) = thread[index].r12;
    *((unsigned int*) handler_sp+13) = thread[index].r13;  
    *((unsigned int*) handler_sp+14) = thread[index].lr_handler; // should jump to thread job
    *((unsigned int*) handler_sp+15) = thread[index].r15;
    *((unsigned int*) handler_sp+16) = thread[index].lr_usr;
    *((unsigned int*) handler_sp+17) = thread[index].sp_usr;
    return thread[index].cpsr;
};

unsigned int swap_thread(unsigned int handler_sp, unsigned int spsr)
{
    int next_thread_index = find_next_thread();
    //kprintf("Swap Thread %i -> % i \n", running_thread, next_thread_index);
    swap_process(thread[next_thread_index].process_id);
    if (thread[running_thread].state == LAUFEND)
    {
        thread[running_thread].state = WARTEND;
        save_context(handler_sp, spsr);
    };
    unsigned int new_spsr = load_context(next_thread_index, handler_sp);
    running_thread = next_thread_index;
    thread[running_thread].state = LAUFEND;
    return new_spsr;    
};

void init_idle_thread()
{
    thread_create(&idle_thread_job, (void*) thread[0].thread_sp, 0);
    //kprintf("Init idle thread: %p\n", &idle_thread_job);
};

void init_creator_thread()
{
    thread_create(&creator_thread_job, (void*) thread[1].thread_sp, 0);
    //kprintf("Init creator thread: %p\n", &creator_thread_job);
}


// Ultimate Scheduler
int find_next_thread()
{   
    if (running_thread == -1) return 0;
    int i = (running_thread == number_of_threads - 1) ? 1 : running_thread + 1;
    int counter = 0;
    while (i != running_thread)
    {
        if (thread[i].state == BEREIT) return i;
        i = (i == number_of_threads - 1) ? 1 : i + 1;
        counter++;
        if (counter == number_of_threads - 2) break;        
    };
    return (thread[running_thread].state == BEREIT) ? running_thread : IDLE_THREAD; 
};