#include <interrupt_reg.h>
#include <kprintf.h>
#include <local_timer.h>

void kernel_stop()
{
    kprintf("\nException occurs in Kernel. Kernel is stopped\n");
    disable_timer_interrupt();
    disable_uart_irq();
    while (1) {};      
}