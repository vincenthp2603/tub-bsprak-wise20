#include <kprintf.h>
#include <exception_print.h>
#include <local_timer.h>
#include <handler.h>
#include <interrupt_reg.h>
#include <uart.h>
#include <specific_regs_access.h>
#include <thread.h>
#include <user_thread.h>
#include <uart_input_handler.h>
#include <kernel_stop.h>
//#include <mmu_kernel_test.h>
#include <mmu.h>
#include <mem.h>
#include <process.h>
#include "../../../user/include/syscall.h"

#define SYSCALL_END_THREAD 0
#define SYSCALL_CREATE_THREAD 1
#define SYSCALL_SLEEP_THREAD 2
#define SYSCALL_UART_PRINT 3
#define SYSCALL_UART_READ 4
#define SYSCALL_CREATE_PROCESS 5

#define NULL 0x0

int tick_ctr = 0;

void reset_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr)
{
    exception_print(sp, cpsr, spsr, Reset);
    int running = get_running_thread();
    if (get_psr_mode(spsr) == mode_user && running != IDLE_THREAD)
    {
        tcb *thread = get_tcbs_list();
        thread[running].state = BEENDET;
        swap_thread(sp, spsr);
    };
};

unsigned int undefined_instruction_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr)
{
    exception_print(sp, cpsr, spsr, Undefined);
    if (get_psr_mode(spsr) == mode_user)
    {
        tcb *thread = get_tcbs_list();
        int current = get_running_thread();
        thread[current].state = BEENDET;
        return swap_thread(sp, spsr);
    }
    else
    {
        kernel_stop();
    }
    return spsr;
};

unsigned int software_interrupt_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr)
{
    if (get_psr_mode(spsr) != mode_user)
        exception_print(sp, cpsr, spsr, Software_Interrupt);
    if (get_psr_mode(spsr) == mode_user)
    {
        unsigned int swi_intr_addr = (*(unsigned int *)(sp + 14 * 4)) + get_pc_offset(Software_Interrupt);
        unsigned int swi_intr = *(unsigned int *)swi_intr_addr;
        unsigned int syscall_number = (swi_intr << 24) >> 24;

        unsigned int to_print;
        unsigned int to_read;

        int new_thread_id;
        int new_process_id;

        void *arg = (void *)*((unsigned int *)sp + 1);
        unsigned int arg_size = *((unsigned int *)sp + 2);

        tcb *thread = get_tcbs_list();
        int current = get_running_thread();
        unsigned int new_spsr;

        switch (syscall_number)
        {
        case SYSCALL_END_THREAD:
            thread[current].state = BEENDET;
            return swap_thread(sp, spsr);

        case SYSCALL_CREATE_THREAD:
            new_thread_id = thread_create((void *)*((unsigned int *)sp), (void *)*((unsigned int *)sp + 1), *((unsigned int *)sp + 2));
            if (new_thread_id > 1)
                thread[new_thread_id].process_id = get_current_process();
            break;

        case SYSCALL_CREATE_PROCESS:
            new_process_id = find_next_process();
            if (new_process_id != -1)
            {
                new_thread_id = thread_create((void *)*((unsigned int *)sp), (void *)*((unsigned int *)sp + 1), *((unsigned int *)sp + 2));
                if (new_thread_id > 1)
                {
                    thread[new_thread_id].process_id = new_process_id;
                    setup_first_user_thread_in_process(new_thread_id, arg, arg_size);
                }
            }
            break;

        case SYSCALL_SLEEP_THREAD:
            thread[current].wait_time = *(unsigned int *)sp;
            new_spsr = swap_thread(sp, spsr);
            return new_spsr;

        case SYSCALL_UART_PRINT:
            to_print = *(unsigned int *)sp;
            uart_transmit((char)to_print);
            break;

        case SYSCALL_UART_READ:
            to_read = pop_uart_buffer();
            *(unsigned int *)sp = to_read;
            break;

        default:
            kprintf("\nUnbekannter SYSCALL: Der laufende Thread wird beendet \n");
            kprintf("Unbekannter SYSCALL: Registerausgabe \n");
            exception_print(sp, cpsr, spsr, Software_Interrupt);
            thread[current].state = BEENDET;
            return swap_thread(sp, spsr);
            break;
        }
    }
    else
    {
        kernel_stop();
    }
    return spsr;
};

unsigned int prefetch_abort_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr)
{
    if (get_running_thread() != -1)
        exception_print(sp, cpsr, spsr, Prefetch_Abort);
    if (get_psr_mode(spsr) == mode_user)
    {
        tcb *thread = get_tcbs_list();
        int current = get_running_thread();
        //kprintf("Exception in Thread %i -> lr_usr %x, pc_usr %x \n", current, *((unsigned int*) sp+16), *((unsigned int*) sp+14));
        thread[current].state = BEENDET;
        return swap_thread(sp, spsr);
    }
    else
    {
        kernel_stop();
    }
    return spsr;
};

unsigned int data_abort_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr)
{
    exception_print(sp, cpsr, spsr, Data_Abort);
    if (get_psr_mode(spsr) == mode_user)
    {
        tcb *thread = get_tcbs_list();
        int current = get_running_thread();
        thread[current].state = BEENDET;
        return swap_thread(sp, spsr);
    }
    else
    {
        kernel_stop();
    }
    return spsr;
};

unsigned int irq_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr)
{
    if (0)
        exception_print(sp, cpsr, spsr, IRQ_Interrupt);

    // UART Interrupt Handler
    if (interrupt_port->irq_pending_2 & (1 << IRQ_UART))
    {
        interrupt_port->disable_irq_2 = 1 << IRQ_UART;
        unsigned int c = uart_receive();
        push_uart_buffer(c);
        interrupt_port->enable_irq_2 = 1 << IRQ_UART;
        return spsr;
    }

    // Timer Interrupt Handler
    if (timer_port->control_and_status >> 31)
    {
        clear_timer_interrupt_flag();
        //kprintf("!");

        tcb *thread = get_tcbs_list();
        for (int i = 1; i < number_of_threads; i++)
        {
            if (thread[i].state == WARTEND)
            {
                if (thread[i].wait_time > 0)
                    thread[i].wait_time--;
                if (thread[i].wait_time == 0)
                {
                    thread[i].state = BEREIT;
                    //if (i == 1) kprintf("Creator is Ready \n");
                }
            }
        }

        return swap_thread(sp, spsr);
    };

    return spsr;
};

unsigned int fiq_handler(unsigned int sp, unsigned int cpsr, unsigned int spsr)
{
    exception_print(sp, cpsr, spsr, FIQ_Interrupt);
    if (get_psr_mode(spsr) == mode_user)
    {
        tcb *thread = get_tcbs_list();
        int current = get_running_thread();
        thread[current].state = BEENDET;
        swap_thread(sp, spsr);
        return spsr;
    };
    return spsr;
};

void switch_irq_debug_mode()
{
    irq_debug_mode = 1 - irq_debug_mode;
};

void switch_interactive_program_mode()
{
    interactive_program = 1 - interactive_program;
};

/* Helper functions */
int get_psr_mode(unsigned int psr)
{
    switch (psr % 32)
    {
    case 16:
        return mode_user;
        break;
    case 17:
        return mode_fiq;
        break;
    case 18:
        return mode_irq;
        break;
    case 19:
        return mode_supervisor;
        break;
    case 23:
        return mode_abort;
        break;
    case 27:
        return mode_undefined;
        break;
    case 31:
        return mode_system;
        break;
    default:
        return mode_invalid;
        break;
    }
};

char *get_exception_name(int exception_type)
{
    switch (exception_type)
    {
    case Reset:
        return "Reset";
        break;
    case Undefined:
        return "Undefined";
        break;
    case Software_Interrupt:
        return "Software Interrupt";
        break;
    case Prefetch_Abort:
        return "Prefetch Abort";
        break;
    case Data_Abort:
        return "Data Abort";
        break;
    case IRQ_Interrupt:
        return "IRQ Interrupt";
        break;
    case FIQ_Interrupt:
        return "FIQ_Interrupt";
        break;
    default:
        return "";
        break;
    }
};

char *get_psr_flags(unsigned int psr)
{
    char *outputStr = "____ _ ___";
    char *ptr = outputStr;

    while (*ptr != 0)
    {
        if (*ptr != ' ')
            *ptr = '_';
        ptr++;
    };

    if ((psr & (1 << N)) >> N == 1)
        outputStr[0] = 'N';
    if ((psr & (1 << Z)) >> Z == 1)
        outputStr[1] = 'Z';
    if ((psr & (1 << C)) >> C == 1)
        outputStr[2] = 'C';
    if ((psr & (1 << V)) >> V == 1)
        outputStr[3] = 'V';
    if ((psr & (1 << E)) >> E == 1)
        outputStr[5] = 'E';
    if ((psr & (1 << I)) >> I == 1)
        outputStr[7] = 'I';
    if ((psr & (1 << F)) >> F == 1)
        outputStr[8] = 'F';
    if ((psr & (1 << T)) >> T == 1)
        outputStr[9] = 'T';
    return outputStr;
};

char *get_mode_str(int mode)
{
    switch (mode)
    {
    case mode_user:
        return "User";
        break;
    case mode_system:
        return "System";
        break;
    case mode_undefined:
        return "Undefined";
        break;
    case mode_supervisor:
        return "Supervisor";
        break;
    case mode_abort:
        return "Abort";
        break;
    case mode_fiq:
        return "FIQ";
        break;
    case mode_irq:
        return "IRQ";
        break;
    default:
        return "Invalid";
        break;
    }
};

int get_pc_offset(int exception_type)
{
    if (exception_type == Undefined || exception_type == Software_Interrupt || exception_type == Prefetch_Abort)
        return -4;
    else if (exception_type == Data_Abort || exception_type == IRQ_Interrupt || exception_type == FIQ_Interrupt)
        return -8;
    else
        return 0;
};

void print_data_abort_description()
{
    unsigned int dfar = get_dfar();
    unsigned int dfsr = get_dfsr();
    char *accessType;
    char *error;
    accessType = (dfsr & (1 << DFSR_RW)) ? "schreibend" : "lesend";

    switch (dfsr % 32)
    {
    case 0:
        error = "No function, reset value";
        break;
    case 1:
        error = "Alignment fault";
        break;
    case 2:
        error = "Debug event fault";
        break;
    case 3:
        error = "Access Flag fault on Section";
        break;
    case 4:
        error = "Cache maintenance operation fault[b]";
        break;
    case 5:
        error = "Translation fault on Section";
        break;
    case 6:
        error = "Access Flag fault on Page";
        break;
    case 7:
        error = "Translation fault on Page";
        break;
    case 8:
        error = "Precise External Abort";
        break;
    case 9:
        error = "Domain fault on Section";
        break;
    case 10:
        error = "No function";
        break;
    case 11:
        error = "Domain fault on Page";
        break;
    case 12:
        error = "External abort on translation, first level";
        break;
    case 13:
        error = "Permission fault on Section";
        break;
    case 14:
        error = "External abort on translation, second level";
        break;
    case 15:
        error = "Permission fault on Page";
        break;
    case 22:
        error = "Imprecise External Abort";
        break;
    default:
        error = "No function";
    };

    kprintf("Zugriff: %s auf Adresse 0x%08x \nFehler: %s \n", accessType, dfar, error);
};

void print_reg(unsigned int reg)
{
    kprintf("Reg is %x -> %x \n", reg, *((unsigned int *)(reg - 1)));
};
