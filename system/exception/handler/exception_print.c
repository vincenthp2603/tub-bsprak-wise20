#include <kprintf.h>
#include <string.h>
#include <exception_print.h>
#include <specific_regs_access.h>
#include <handler.h>

void exception_print(unsigned int sp, unsigned int cpsr, unsigned int spsr, int exception_type){
    
    char* exception_name = get_exception_name(exception_type);

    kprintf("\n");
    kprintf("############################################################################\n");
    kprintf("\n%s an Adresse 0x%08x\n", exception_name,*(int*)(sp+14*4)+ get_pc_offset(exception_type));

    kprintf("\n>>> Registerschnappschuss (aktueller Modus) <<< \n");
    for (int i = 0; i < 8; i++) {
        kprintf("R%-2u: 0x%08x    ",i   , *(int*) (sp+i*4));
        kprintf("R%-2u: 0x%08x  \n",i+8 , *(int*) (sp+(i+8)*4));
    }
    if (exception_type == Data_Abort) print_data_abort_description();
    
    kprintf("\n>>> Aktuelle Statusregister (SPSR des aktuellen Modus) <<< \n");
    kprintf("CPSR: %s %-12s (0x%08x)\n", get_psr_flags(cpsr), get_mode_str(get_psr_mode(cpsr)), cpsr);
    kprintf("SPSR: %s %-12s (0x%08x)\n", get_psr_flags(spsr), get_mode_str(get_psr_mode(spsr)), spsr);

    unsigned int lr_user, sp_user;
    unsigned int lr_supervisor, sp_supervisor, spsr_supervisor;
    unsigned int lr_abort, sp_abort, spsr_abort;
    unsigned int lr_fiq, sp_fiq, spsr_fiq;
    unsigned int lr_irq, sp_irq, spsr_irq;
    unsigned int lr_undefined, sp_undefined, spsr_undefined;

    int mode = get_psr_mode(cpsr);

    lr_user = mode == mode_user || mode == mode_system ? get_lr() : get_lr_user();
    sp_user = mode == mode_user || mode == mode_system ? get_sp() : get_sp_user();

    lr_supervisor = mode == mode_supervisor ? get_lr() : get_lr_supervisor();
    sp_supervisor = mode == mode_supervisor  ? get_sp() : get_sp_supervisor();
    spsr_supervisor = mode == mode_supervisor ? get_spsr() : get_spsr_supervisor(); 
    
    lr_abort  = mode == mode_abort ? get_lr() : get_lr_abort();
    sp_abort = mode == mode_abort ? get_sp() : get_sp_abort();
    spsr_abort = mode == mode_abort ? get_spsr() : get_spsr_abort();
    
    lr_fiq = mode == mode_fiq ? get_lr() : get_lr_fiq();
    sp_fiq = mode == mode_fiq ? get_sp() : get_sp_fiq();
    spsr_fiq = mode == mode_fiq ? get_spsr() : get_spsr_fiq();

    lr_irq = mode == mode_irq ? get_lr() : get_lr_irq();
    sp_irq = mode == mode_irq ? get_sp() : get_sp_irq();
    spsr_irq = mode == mode_irq ? get_spsr() : get_spsr_irq();

    lr_undefined = mode == mode_undefined ? get_lr() : get_lr_undefined();
    sp_undefined = mode == mode_undefined ? get_sp() : get_sp_undefined();
    spsr_undefined = mode == mode_undefined ? get_spsr() : get_spsr_undefined();

    kprintf("\n>>> Aktuelle modusspezifische Register <<<");
    kprintf("\n             LR          SP         SPSR");
    kprintf("\nUser/System: 0x%08x  0x%08x", lr_user, sp_user);
    kprintf("\nSupervisor:  0x%08x  0x%08x %s %-12s (0x%08x)", lr_supervisor, sp_supervisor, get_psr_flags(spsr_supervisor), get_mode_str(get_psr_mode(spsr_supervisor)), spsr_supervisor);
    kprintf("\nAbort:       0x%08x  0x%08x %s %-12s (0x%08x)", lr_abort, sp_abort, get_psr_flags(spsr_abort), get_mode_str(get_psr_mode(spsr_abort)), spsr_abort);
    kprintf("\nFIQ:         0x%08x  0x%08x %s %-12s (0x%08x)", lr_fiq, sp_fiq, get_psr_flags(spsr_fiq), get_mode_str(get_psr_mode(spsr_fiq)), spsr_fiq);
    kprintf("\nIRQ:         0x%08x  0x%08x %s %-12s (0x%08x)", lr_irq, sp_irq, get_psr_flags(spsr_irq), get_mode_str(get_psr_mode(spsr_irq)), spsr_irq);
    kprintf("\nUndefined:   0x%08x  0x%08x %s %-12s (0x%08x)", lr_undefined, sp_undefined, get_psr_flags(spsr_undefined), get_mode_str(get_psr_mode(spsr_undefined)), spsr_undefined);
    kprintf("\n\n");
};

