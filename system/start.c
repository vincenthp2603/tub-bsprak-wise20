#include <led.h>
#include <mmu.h>
#include <config.h>
#include <local_timer.h>
#include <interrupt_reg.h>
#include <thread.h>
#include <uart.h>
#include <kprintf.h>
#include <process.h>

void start_kernel(void)
{
	interrupt_driven_uart_setup();
	enable_irq();
	init_mmu();
	init_timer();
	init_processes();
	kprintf("\n--- Init done, for best Testing experiences wait for a little bit more :P --- \n\n");
	while (1) {}
};
