#include <mmu.h>
#include <mmu_controller.h>
#include <kprintf.h>
#include <process.h>

static unsigned int L1_table[L1_TABLE_SIZE] __attribute__((aligned(L1_ALIGNMENT)));
static unsigned int L2_tables[number_of_processes][L2_TABLE_SIZE];

int PHYS_USER_STACK_SECS[] = {0x1C, 0x1D, 0x1E, 0x1F, 0x20, 0x21, 0x22, 0x23, 0x24};
int PHYS_USER_DATA_SECS[] = {0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F};

int get_phys_user_stack_sec(int index) {
    return PHYS_USER_STACK_SECS[index];
}

unsigned int get_L1_table_base_addr() {
    return (unsigned int) L1_table;
}

unsigned int get_L2_table_base_addr(int index) {
    return (unsigned int) L2_tables[index];
}

/*------------------------------ L1 TABLE ------------------------------*/
void L1_init_section_entry(unsigned int index)
{
    switch (index) {
        case VIR_USER_BSS_SEC:
            L1_table[index] = (VIR_USER_BSS_SEC + get_current_process() + 1) << 20;
            break;
        case VIR_USER_DATA_SEC:
            L1_table[index] = (VIR_USER_DATA_SEC + get_current_process() + 1) << 20;
            break;
        case VIR_USER_STACK_SEC:
            L1_table[index] = (VIR_USER_STACK_SEC + get_current_process() + 1) << 20;        
            break;
        default:
            L1_table[index] = (index << 20);
    }
    L1_table[index] |= (1 << L1_SECTION_TYPE);
}

void L1_init_L2_pointer_entry(unsigned int index, unsigned int L2_base_addr)
{
    //if (index == VIR_USER_STACK_SEC) index += (get_current_process() + 1); 
    L1_table[index] = (L2_base_addr >> 10) << 10;
    L1_table[index] |= 1;
}

void L1_invalidate_entry(unsigned int index)
{
    L1_table[index] = 0;
}

void L1_set_fullAccess_section(unsigned int index)
{
    L1_table[index] |= (1 << L1_SEC_TYPE_AP0);
    L1_table[index] |= (1 << L1_SEC_TYPE_AP1);
    L1_table[index] &= ~(1 << L1_SEC_TYPE_AP2);
}

void L1_set_sys_rw_section(unsigned int index)
{
    L1_table[index] |= (1 << L1_SEC_TYPE_AP0);
    L1_table[index] &= ~(1 << L1_SEC_TYPE_AP1);
    L1_table[index] &= ~(1 << L1_SEC_TYPE_AP2);
}

void L1_set_sys_r_section(unsigned int index)
{
    L1_table[index] |= (1 << L1_SEC_TYPE_AP0);
    L1_table[index] &= ~(1 << L1_SEC_TYPE_AP1);
    L1_table[index] |= (1 << L1_SEC_TYPE_AP2);
}

void L1_set_execNever_section(unsigned int index)
{
    L1_table[index] |= (1 << L1_SEC_TYPE_XN);
}

void L1_set_privileged_execNever_section(unsigned int index)
{
    L1_table[index] |= (1 << L1_SEC_TYPE_PXN);
}

void L1_set_usr_r_section(unsigned int index)
{
    L1_table[index] &= ~(1 << L1_SEC_TYPE_AP0);
    L1_table[index] |= (1 << L1_SEC_TYPE_AP1);
    L1_table[index] &= ~(1 << L1_SEC_TYPE_AP2);
}

void L1_set_privileged_execNever_ptr(unsigned int index)
{
    L1_table[index] |= (1 << L1_PTR_TYPE_PXN);
}

/*------------------------------ L2 TABLE ------------------------------*/
void L2_init_entry(unsigned int table_index, unsigned int entry_index)
{
    unsigned int L1_index = VIR_USER_STACK_SEC /*+ get_current_process()*/ + 1 + table_index; // Mapping for user stacks                                                           
    unsigned int page_addr = L1_index << 8 | entry_index;
    L2_tables[table_index][entry_index] = (page_addr << 12) | (1 << L2_PAGE_PTR_TYPE);
}

void L2_invalidate_entry(unsigned int table_index, unsigned int entry_index)
{
    L2_tables[table_index][entry_index] = 0;
}

void L2_set_fullAccess_entry(unsigned int table_index, unsigned int entry_index)
{
    L2_tables[table_index][entry_index] |= (1 << L2_AP0);
    L2_tables[table_index][entry_index] |= (1 << L2_AP1);
    L2_tables[table_index][entry_index] &= ~(1 << L2_AP2);
}

void L2_set_execNever_entry(unsigned int table_index, unsigned entry_index)
{
    L2_tables[table_index][entry_index] |= 1;
}


/*---------------------------- TABLES SETUP ----------------------------*/
void setup_L2_tables()
{
    for (int i = 0; i < number_of_processes; i++)
    {
        for (int j = 0; j < L2_TABLE_SIZE; j++) 
        {
            L2_init_entry(i,j);
            L2_set_fullAccess_entry(i,j);
            L2_set_execNever_entry(i,j);
        }
    }       
}

void setup_L2_table_of_process(int index) {
    for (int i = 0; i < L2_TABLE_SIZE; i++) {
        L2_init_entry(index, i);
        L2_set_fullAccess_entry(index,i);
        L2_set_execNever_entry(index,i);
    }
}

void setup_L1_table()
{
    for (int i = 0; i < L1_TABLE_SIZE; i++)
    {
        L1_invalidate_entry(i);
        //L1_init_section_entry(i);
        //L1_set_fullAccess_section(i);           
    }
    
    L1_init_section_entry(PERIPHERAL_SEC_0);
    L1_set_execNever_section(PERIPHERAL_SEC_0);
    L1_set_sys_rw_section(PERIPHERAL_SEC_0);

    L1_init_section_entry(PERIPHERAL_SEC_1);
    L1_set_execNever_section(PERIPHERAL_SEC_1);
    L1_set_sys_rw_section(PERIPHERAL_SEC_1);

    L1_init_section_entry(PERIPHERAL_SEC_2);
    L1_set_execNever_section(PERIPHERAL_SEC_2);
    L1_set_sys_rw_section(PERIPHERAL_SEC_2);

    L1_init_section_entry(KERNEL_INIT_SEC);
    L1_set_sys_r_section(KERNEL_INIT_SEC);

    L1_init_section_entry(KERNEL_TEXT_SEC);
    L1_set_sys_r_section(KERNEL_TEXT_SEC);
    
    L1_init_section_entry(KERNEL_DATA_SEC);
    L1_set_sys_rw_section(KERNEL_DATA_SEC);
    L1_set_execNever_section(KERNEL_DATA_SEC);

    L1_init_section_entry(KERNEL_STACK_SEC);
    L1_set_sys_rw_section(KERNEL_STACK_SEC);
    L1_set_execNever_section(KERNEL_STACK_SEC);
    
    L1_init_section_entry(USER_MODE_STACK_SEC);
    L1_set_fullAccess_section(USER_MODE_STACK_SEC);
    L1_set_execNever_section(USER_MODE_STACK_SEC);

    L1_init_section_entry(USER_TEXT_SEC);
    L1_set_usr_r_section(USER_TEXT_SEC);
    L1_set_privileged_execNever_section(USER_TEXT_SEC);

    L1_init_section_entry(USER_RODATA_SEC);
    L1_set_usr_r_section(USER_RODATA_SEC);
    L1_set_execNever_section(USER_RODATA_SEC);
        
    L1_init_section_entry(VIR_USER_DATA_SEC);
    L1_set_fullAccess_section(VIR_USER_DATA_SEC);
    L1_set_execNever_section(VIR_USER_DATA_SEC);

    L1_init_section_entry(VIR_USER_BSS_SEC);
    L1_set_fullAccess_section(VIR_USER_BSS_SEC);
    L1_set_execNever_section(VIR_USER_BSS_SEC);

    L1_init_section_entry(VIR_USER_STACK_SEC);
    L1_set_fullAccess_section(VIR_USER_STACK_SEC);
    L1_set_execNever_section(VIR_USER_STACK_SEC);   

    L1_init_L2_pointer_entry(VIR_USER_STACK_SEC, get_L2_table_base_addr(0));
    L1_set_privileged_execNever_ptr(VIR_USER_STACK_SEC);
    setup_L2_table_of_process(0);

    for (int i = 0; i < number_of_processes; i++) {
        L1_init_section_entry(PHYS_USER_STACK_SECS[i]);
        L1_set_sys_rw_section(PHYS_USER_STACK_SECS[i]);
    
        L1_init_section_entry(PHYS_USER_DATA_SECS[i]);
        L1_set_sys_rw_section(PHYS_USER_DATA_SECS[i]);
    }

};


/*------------------------------ INIT MMU ------------------------------*/
void init_sctlr()
{   
    unsigned int sctlr = get_sctlr();
    sctlr &= ~(1 << C_CACHE_EN); // disable C_Cache
    sctlr &= ~(1 << I_CACHE_EN); // disable I_Cache
    //sctlr |= (1 << ALIGNMENT_CHECK_EN);
    sctlr |= (1 << MMU_EN);      // enable MMU
    set_sctlr(sctlr);
    flush_tlb();
}

void init_mmu() 
{
    init_dacr(DACR_CLIENT);
    //setup_L2_tables();
    setup_L1_table();
    init_ttbcr(TTBCR_USE_TTBR0);
    init_ttbr0(L1_table);
    init_sctlr();
}